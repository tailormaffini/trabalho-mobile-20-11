package com.example.tailo.trabalho1;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText editText;
    SharedPreferences prefs;
    Intent i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editText = findViewById(R.id.editText);
        i = new Intent(this, CalculoActivity.class);
        prefs = getSharedPreferences("appAula", MODE_PRIVATE);
        String restoredText = prefs.getString("nome", null);
        if (restoredText != null) {
            //startActivity(i);
        }
    }

    public void clickCalcular(View view){
        if( editText.getText().toString().equals("")){
            Toast t = Toast.makeText(this, getResources().getString(R.string.strNomeNulo), Toast.LENGTH_LONG);
            t.show();
        } else{
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString("nome", editText.getText().toString());
            editor.commit();
            startActivity(i);
        }


    }
}
