package com.example.tailo.trabalho1;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class ResultadoActivity extends AppCompatActivity {
    TextView textResultado;
    SharedPreferences prefs;
    Intent i;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultado);

        textResultado = findViewById(R.id.textView5);

        prefs = getSharedPreferences("appAula", MODE_PRIVATE);
        String restoredText = prefs.getString("resultado", null);
        textResultado.setText(restoredText);

    }
}
