package com.example.tailo.trabalho1;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class CalculoActivity extends AppCompatActivity {
    EditText editAltura, editPeso;
    SharedPreferences prefs;
    Intent i, i2;

    TextView textOla;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculo);

        i = new Intent(this, MainActivity.class);
        i2 = new Intent(this, ResultadoActivity.class);
        editAltura = findViewById(R.id.editText2);
        editPeso = findViewById(R.id.editText3);
        textOla = findViewById(R.id.textView2);
        prefs = getSharedPreferences("appAula", MODE_PRIVATE);
        String restoredText = prefs.getString("nome", null);
        textOla.setText(textOla.getText().toString() + " " + restoredText + "!");
    }
    public void clickRename(View view){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("nome", "");
        editor.commit();
        startActivity(i);
    }
    public void clickCalcular(View view){
        String altura = editAltura.getText().toString();
        String peso = editPeso.getText().toString();
        double resultado = 0;
        if(altura.isEmpty() || peso.isEmpty()){
            Toast t = Toast.makeText(this, getResources().getString(R.string.strCampoNulo), Toast.LENGTH_LONG);
            t.show();
        } else{
            try {
                resultado = Double.parseDouble(peso) / (Double.parseDouble(altura) * Double.parseDouble(altura));
            } catch(Exception e){
                Toast t = Toast.makeText(this, getResources().getString(R.string.strErro), Toast.LENGTH_LONG);
                t.show();
            }
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString("resultado", Double.toString(resultado));
            editor.commit();
            startActivity(i2);
        }
    }
}
